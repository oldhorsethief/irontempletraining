import os
from flask import Flask, render_template, redirect, request, url_for, send_from_directory, session, flash


app = Flask(__name__)
app.secret_key = "super secret key"

#Display Home page
@app.route('/', methods=["GET", "POST"])
def index():
    the_recipe = mongo.db.recipes.find()
    return render_template("index.html", page_title="Get fit with Fitness Cookbook",  recipe = the_recipe)


# Display Recipes Page
@app.route('/index')
def index():
    return render_template("index.html", page_title="index")


if __name__ == '__main__':
    app.run(host=os.environ.get('IP'),
            port=int(os.environ.get('PORT')),
            debug=True)